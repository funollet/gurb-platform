class r_web {

    package { 'rails':
        provider => gem,
        ensure   => '3.2.0',
    }

    # Nginx proxies requests to Unicorn.
    class { 'nginx': }

    nginx::resource::vhost { '_':
        ensure => present,
        proxy  => 'http://gurb_app',
    }

    nginx::resource::upstream { 'gurb_app':
        ensure  => present,
        members => [
            'localhost:8080 fail_timeout=0',
        ],
    }

    class { 'unicorn':
        app_path  => '/home/ubuntu/gurb-app',
        app_user  => 'ubuntu',
        app_group => 'ubuntu',
    }

    user { "gurb":
        comment    => 'gurb app',
        uid        => 666,
        managehome => true,
        home       => '/home/gurb',
        password   => sha1(hiera('gurb_password')),
        shell      => '/bin/bash',
    }
}
