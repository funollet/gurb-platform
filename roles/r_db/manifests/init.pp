class r_db {
    class { 'mysql': }
    class { 'mysql::server':
        manage_service => false,
        config_hash    => { 'root_password' => hiera('mysql_root_password', '') }
    }
    class { 'mysql::ruby': }

    mysql::db { 'gurb':
        user     => 'gurb',
        password => hiera('mysql_gurb_password'),
        host     => 'localhost',
        grant    => ['all'],
    }
}
