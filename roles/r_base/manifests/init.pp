class r_base {
        
    # Useful packages.
    package { ['iproute', 'sysv-rc-conf', 'mutt', 'bsd-mailx', 'tree', 'colordiff',
        'dstat', 'htop', 'less', 'lshw', 'hwinfo', 'screen', 'ethtool', 'psmisc',
        'pwgen', 'rsync', 'lsb-release', 'python-pygments', 'curl',
        'zip', 'unzip', 'binutils', 'iptraf', 'bwm-ng',
        'util-linux', 'strace', 'lsof', 'debsums']:
        ensure => installed,
    }
    
    package { ['command-not-found', 'command-not-found-data']:
        ensure => purged,
    }

    package { ['vim', 'vim-scripts']:
        ensure    => present,
    }
    # Set default editor.
    exec { 'update-alternatives --set editor /usr/bin/vim.basic':
        path        => '/bin:/sbin:/usr/bin:/usr/sbin',
        require     => Package['vim'],
        subscribe   => Package['vim'],
        refreshonly => true,
    }

    class { 'ntp': }
}
