
Requirements
============

 - Checkout repository on your workstation.

       ssh://git@bitbucket.org/funollet/gurb-platform.git"

 - Define AWS configuration as environment variables.

       export EC2_URL=https://ec2.eu-west-1.amazonaws.com
       export AWS_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXX
       export AWS_SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

 - Create file `hieradata/secrets.yaml` to provide passwords out of revision control.
   See example file in `hieradata/`.


Provisioning and configuration
==============================

Setup EC2 security groups.

    rake ec2:init       # Create EC2 project environment.

Create a new EC2 instance.

    rake ec2:provision  # Create new EC2 instances.

Put Capistrano's roles/servers config in `config/cap_nodes.rb`. Example:

    server "ec2-54-216-146-85.eu-west-1.compute.amazonaws.com", :web

Put Puppet configuration on the server.

    cap puppet:init

Update Puppet config and apply.

    cap puppet:apply


At this point the host is ready for deploying the application.


Deployment
==========

A minimalistic application is included just to test that Nginx + unicorn are working.

Before deploying the application,

    cap deploy:setup

Deploy new code:

    cap deploy:update

Test with `http://<server-name>/`.

