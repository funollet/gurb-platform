# Key pair used to log into your nodes. Adjust as needed.
$EC2_KEY_PAIR='id_rsa'

$AMI_UBUNTU='ami-89b1a3fd'      # Ubuntu 12.04 precise LTS (EBS) from alestic.com
# $AMI_UBUNTU='ami-b3aebdc7'    # Ubuntu 13.04 raring (EBS) from alestic.com


namespace :ec2 do
    desc "Create EC2 project environment.
    
    Defines security groups that our platform will use.
    Run just once, before creating any EC2 instances."
    task :init do
        sh "ec2-create-group gurb.web -d 'web servers'"
        sh "ec2-authorize gurb.web -P tcp -p 22 -s 0.0.0.0/0"
        sh "ec2-authorize gurb.web -P tcp -p 80 -s 0.0.0.0/0"

        sh "ec2-create-group gurb.db -d 'mysql servers'"
        sh "ec2-authorize gurb.db -P tcp -p 22 -s 0.0.0.0/0"
        sh "ec2-authorize gurb.db -P tcp -p 3306 -o gurb.web"
    end

    task :clean do
        sh "ec2-delete-group gurb.db"
        sh "ec2-delete-group gurb.web"
    end

    desc "Create new EC2 instances."
    task :provision do
        out = %x{ec2-run-instances #{$AMI_UBUNTU} --key #{$EC2_KEY_PAIR} --instance-type t1.micro  --group gurb.web --group gurb.db --user-data-file tools/bootstrap.sh}
        puts out

        # Tag new instances.
        out.split("\n").select { |l| l =~ /^INSTANCE/ }.each do |info|
            instance_id = info.split()[1]
            sh "ec2tag #{instance_id} --tag Project=gurb --tag Roles=web,db"
        end
    end
end
