#!/bin/bash
# bootstrap.sh
#
# This EC2 bootstrap script installs puppet and dependencies.

# Let's use ruby-1.9.3. Rails requires ruby > 1.9.1.
aptitude update
aptitude -y install build-essential ruby1.9.1 ruby1.9.1-dev
update-alternatives --set ruby /usr/bin/ruby1.9.1
update-alternatives --set gem /usr/bin/gem1.9.1

# No questions while installing packages in a script.
cat > /etc/apt/apt.conf.d/00DpkgOptions <<.
Dpkg::Options {
   "--force-confdef";
   "--force-confold";
}
.

aptitude -y install python-pip git rake

# Install puppet as a gem so 'librarian-puppet' can be used.
gem install puppet -v 2.7.22
useradd -d /var/lib/puppet/ -m -s /bin/false -U puppet

gem install librarian-puppet hiera
gem install hiera-puppet -v 1.0.0
# Make hiera-puppet available as a module, so Puppet can use its functions.
mkdir -p /usr/share/puppet/modules
ln -s /var/lib/gems/1.9.1/gems/hiera-puppet-1.0.0 /usr/share/puppet/modules/hiera-puppet




######################################################################
# Puppet could be installed from Puppetlabs repositories, but gem
# 'librarian-puppet' won't detect puppet.
# https://github.com/rodjek/librarian-puppet/issues/99

# # Use puppet-2.7 instead of 3.x. Not all modules are (yet) ready for puppet-3.
# cat >> /etc/apt/preferences <<.
# Package: puppet
# Pin: version 2.7.22-*
# Pin-priority: 1001
#
# Package: puppet-common
# Pin: version 2.7.22-*
# Pin-priority: 1001
# .
#
# # Get puppet from Puppetlabs repos.
# cd /tmp ; wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb
# dpkg -i /tmp/puppetlabs-release-precise.deb
# aptitude update
# 
# aptitude install puppet hiera-puppet
######################################################################

