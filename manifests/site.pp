# manifests/site.pp

Exec { path => '/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin/:/bin:/sbin' }
case $::operatingsystem {
    'Ubuntu': {
        Package { provider => 'aptitude' }
    }
}

case $::operatingsystem {
    'Ubuntu': {
        file { '/etc/apt/sources.list':
            content => '',
        }
        apt::source {
            'precise':
                location    => 'http://eu-west-1.ec2.archive.ubuntu.com/ubuntu/',
                repos       => 'main restricted universe multiverse',
                include_src => false ;
            'precise-updates':
                location    => 'http://eu-west-1.ec2.archive.ubuntu.com/ubuntu/',
                repos       => 'main restricted universe multiverse',
                release     => 'precise-updates',
                include_src => false ;
            'precise-backports':
                location    => 'http://eu-west-1.ec2.archive.ubuntu.com/ubuntu/',
                repos       => 'main restricted universe multiverse',
                release     => 'precise-backports',
                include_src => false ;
            'precise-security':
                location    => 'http://eu-west-1.ec2.archive.ubuntu.com/ubuntu',
                repos       => 'main restricted universe multiverse',
                release     => 'precise-security',
                include_src => false ;
            # Use puppet packages from Puppetlabs repos.
            'puppetlabs':
                location    => 'http://apt.puppetlabs.com',
                repos       => 'main dependencies',
                include_src => false ;
        }
    }
}

import 'nodes/*.pp'
