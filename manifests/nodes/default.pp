node default {
    include r_base

    # Security groups map to Puppet roles.
    if $::ec2_security_groups =~ /gurb.web/     { include r_web }
    if $::ec2_security_groups =~ /gurb.db/      { include r_db }
}
