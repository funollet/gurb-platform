load "config/cap_nodes"

set :application, "gurb-app"
set :repository,  "ssh://git@bitbucket.org/funollet/gurb-platform.git"
set :repository,  "https://funollet@bitbucket.org/funollet/gurb-platform.git"
set :user,         "ubuntu"
set :ssh_options, { :forward_agent => true }


namespace :puppet do

    desc "Initialize puppet configuration."
    task :init, :roles => :web do
        # Clean old config.
        run "sudo rm -rf /etc/puppet"
        run "sudo mkdir -p /etc/puppet && sudo chown #{user} /etc/puppet"
        run "git clone #{repository} /etc/puppet"
        run "mkdir /etc/puppet/modules"
    end

    desc "Run puppet (updates repository, too)."
    task :apply, :roles => :web do
        run "cd /etc/puppet && git pull"
        upload "hieradata/secrets.yaml", "/etc/puppet/hieradata/secrets.yaml", {:mode => 0600}
        run "cd /etc/puppet && librarian-puppet install"
        run "cd /etc/puppet && sudo puppet apply manifests/site.pp"
    end

    desc "Run puppet with --noop (updates repository, too)."
    task :dryrun, :roles => :web do
        run "cd /etc/puppet && git pull"
        upload "hieradata/secrets.yaml", "/etc/puppet/hieradata/secrets.yaml", {:mode => 0600}
        run "cd /etc/puppet && librarian-puppet install"
        run "cd /etc/puppet && sudo puppet apply manifests/site.pp --noop"
    end
end


# Deploy a minimalistic application to test Nginx + Unicorn.
namespace :deploy do
    desc "Initial setup."
    task :setup, :roles => :web do
        run "cd /home/#{user} && mkdir -p #{application}"
        run "cd /home/#{user}/#{application} && mkdir -p config shared/pids shared/log"
        upload "config.ru", "/home/#{user}/#{application}/config.ru"
        upload "config/unicorn.rb", "/home/#{user}/#{application}/config/unicorn.rb"
        run "sudo start unicorn"
    end

    desc "Update application."
    task :update, :roles => :web do
        upload "config.ru", "/home/#{user}/#{application}/config.ru"
        upload "config/unicorn.rb", "/home/#{user}/#{application}/config/unicorn.rb"
    end
    after "deploy:update", "unicorn:restart"
end

namespace :unicorn do
    desc "Restart Unicorn."
    task :restart, :roles => :web do
        run "kill -HUP $(cat /home/#{user}/#{application}/shared/pids/unicorn.pid)"
    end
end
